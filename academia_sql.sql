CREATE TABLE usuario(
    id SERIAL NOT NULL UNIQUE,
    matricula INTEGER PRIMARY KEY,
    nome VARCHAR(255) NOT NULL,
    sobre_nome VARCHAR(255) NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    genero VARCHAR(1) NOT NULL
);

CREATE TABLE tipo_status_usuario(
    id SERIAL PRIMARY KEY,
    descricao VARCHAR(255) NOT NULL
);

CREATE TABLE status_usuario(
    id SERIAL PRIMARY KEY,
    id_tipo_status_usuario INTEGER NOT NULL,
    id_usuario INTEGER NOT NULL,
    data_inicio DATE NOT NULL,
    data_fim DATE,

    FOREIGN KEY (id_tipo_status_usuario) REFERENCES tipo_status_usuario(id),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);

CREATE SEQUENCE USUARIO_SEQ
  start 5000
  increment 5;

CREATE TABLE turno(
    id SERIAL PRIMARY KEY,
    data_turno DATE NOT NULL,
    hr_inicio TIME NOT NULL,
    hr_fim TIME NOT NULL
);

CREATE TABLE bairro(
    id SERIAL PRIMARY KEY,
    desc_bairro VARCHAR(255) NOT NULL
);

CREATE TABLE cidade(
    id SERIAL PRIMARY KEY,
    desc_cidade VARCHAR(255) NOT NULL,
    id_bairro INTEGER NOT NULL,

    FOREIGN KEY (id_bairro) REFERENCES bairro(id)
);

CREATE TABLE estado(
    id SERIAL PRIMARY KEY,
    desc_estado VARCHAR(255) NOT NULL,
    sigla_estado VARCHAR(2) NOT NULL,
    id_cidade INTEGER NOT NULL,

    FOREIGN KEY (id_cidade) REFERENCES cidade(id)
);

CREATE TABLE endereco(
    id SERIAL NOT NULL UNIQUE,
    id_usuario INTEGER NOT NULL,
    logradouro VARCHAR(255) NOT NULL,
    numero INTEGER NOT NULL,
    complemento VARCHAR(255) NOT NULL,
    cep VARCHAR(8) NOT NULL,
    id_estado INTEGER NOT NULL,

    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    FOREIGN KEY (id_estado) REFERENCES estado(id),

    PRIMARY KEY (id_usuario, id_estado)
);

CREATE TABLE filial(
    id SERIAL PRIMARY KEY,
    nome VARCHAR(255) NOT NULL,
    cnpj VARCHAR(14) NOT NULL,
    id_endereco INTEGER NOT NULL,

    FOREIGN KEY (id_endereco) REFERENCES endereco(id)
);

CREATE TABLE frequenta(
    id SERIAL PRIMARY KEY,
    id_usuario INTEGER NOT NULL,
    id_filial INTEGER NOT NULL,
    id_turno INTEGER NOT NULL,

    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    FOREIGN KEY (id_filial) REFERENCES filial(id),
    FOREIGN KEY (id_turno) REFERENCES turno(id)
);


CREATE TABLE telefone(
    id SERIAL PRIMARY KEY,
    numero VARCHAR(15) NOT NULL,
    ddd VARCHAR(2) NOT NULL,
    codigo_pais VARCHAR(2) NOT NULL
);

CREATE TABLE telefone_filial(
    id SERIAL PRIMARY KEY,
    id_telefone INTEGER NOT NULL,
    id_filial INTEGER NOT NULL,
    tp_telefone VARCHAR(1) NOT NULL,

    FOREIGN KEY (id_telefone) REFERENCES telefone(id),
    FOREIGN KEY (id_filial) REFERENCES filial(id)
);

CREATE TABLE telefone_usuario(
    id SERIAL PRIMARY KEY,
    id_telefone INTEGER NOT NULL,
    id_usuario INTEGER NOT NULL,
    tp_telefone VARCHAR(1) NOT NULL,

    FOREIGN KEY (id_telefone) REFERENCES telefone(id),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);

CREATE TABLE email(
    id SERIAL PRIMARY KEY,
    endereco_email VARCHAR(255) NOT NULL
);

CREATE TABLE email_usuario(
    id SERIAL PRIMARY KEY,
    id_email INTEGER NOT NULL,
    id_usuario INTEGER NOT NULL,
    tp_email VARCHAR(1) NOT NULL,

    FOREIGN KEY (id_email) REFERENCES email(id),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);

CREATE TABLE email_filial(
    id SERIAL PRIMARY KEY,
    id_email INTEGER NOT NULL,
    id_filial INTEGER NOT NULL,
    tp_email VARCHAR(1) NOT NULL,

    FOREIGN KEY (id_email) REFERENCES email(id),
    FOREIGN KEY (id_filial) REFERENCES filial(id)
);



CREATE TABLE cargo(
    id SERIAL PRIMARY KEY,
    desc_cargo VARCHAR(255) NOT NULL,
    nome VARCHAR(255) NOT NULL
);


CREATE TABLE funcionario(
    id SERIAL NOT NULL UNIQUE,
    id_usuario INTEGER NOT NULL,
    id_cargo INTEGER NOT NULL,
    numero_carteira_tabalho INTEGER NOT NULL PRIMARY KEY,

    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    FOREIGN KEY (id_cargo) REFERENCES cargo(id)

);

CREATE SEQUENCE FUNCIONARIO_SEQ
  start 6000
  increment 7;

CREATE TABLE professor(
    id SERIAL PRIMARY KEY,
    id_funcionario INTEGER NOT NULL,
    salario FLOAT NOT NULL,

    FOREIGN KEY (id_funcionario) REFERENCES funcionario(id)

);

CREATE TABLE aluno(
    id SERIAL PRIMARY KEY,
    id_usuario INTEGER NOT NULL,

    FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);

CREATE TABLE aula(
    id SERIAL PRIMARY KEY,
    id_professor INTEGER NOT NULL,
    id_aluno INTEGER NOT NULL,

    FOREIGN KEY (id_professor) REFERENCES professor(id),
    FOREIGN KEY (id_aluno) REFERENCES aluno(id)
);

CREATE TABLE tipo_pagamento(
    id SERIAL NOT NULL PRIMARY KEY,
    tipo_pagamento VARCHAR(50) NOT NULL,
    desc_tipo_pagamento VARCHAR(255) NOT NULL
);

CREATE TABLE tipo_plano(
    id SERIAL PRIMARY KEY,
    desc_plano VARCHAR(255) NOT NULL,
    valor_plano FLOAT NOT NULL
);

CREATE TABLE plano(
    id SERIAL PRIMARY KEY,
    id_tipo_plano INTEGER NOT NULL,
    status_plano VARCHAR(1) NOT NULL,

    FOREIGN KEY (id_tipo_plano) REFERENCES tipo_plano(id)
);

CREATE TABLE contrato(
    id SERIAL NOT NULL UNIQUE,
    id_plano INTEGER NOT NULL,
    id_aluno INTEGER NOT NULL,
    numero_contrato INTEGER PRIMARY KEY,
    id_forma_pagamento INTEGER NOT NULL,
    dt_inicial_contrato DATE NOT NULL,
    dt_final_contrato DATE,

    FOREIGN KEY (id_plano) REFERENCES plano(id),
    FOREIGN KEY (id_aluno) REFERENCES aluno(id),
    FOREIGN KEY (id_forma_pagamento) REFERENCES tipo_pagamento(id)
);

CREATE SEQUENCE CONTRATO_SEQ
  start 8000
  increment 8;

CREATE TABLE forma_pagamento(
    id_forma_pagamento SERIAL NOT NULL PRIMARY KEY,
    id_tipo_pagamento INTEGER NOT NULL,
    id_contrato INTEGER NOT NULL,

    FOREIGN KEY (id_tipo_pagamento) REFERENCES tipo_pagamento(id),
    FOREIGN KEY (id_contrato) REFERENCES contrato(id)
);





